# Backups

## Overview

The backups chart aims to provide an easy, robust, and uniform way to backup deployed application data. Applications typically store data in two locations:

1. flat files in some data volume
2. MySQL or PostgreSQL database

A backup storage volume will be provisioned as an NFS-mounted volume given a server address and path; however, an existing volume of any type can be used by specifying an `existingClaim`.

Incremental snapshots are taken of flat files using `rsnapshot`. Snapshots of files associated with both a data volume as well as the database volume are supported. Additionally, database dumps can be enabled using either `mysqldump` or `pg_dump` depending on the database type.

## Backup

Enable backups by including this chart in your dependencies:

```yaml
apiVersion: v2
name: my-chart
version: 1.0.0
dependencies:
- name: backups
  version: 0.2.x
  repository: https://decentci.gitlab.io/charts
  condition: backups.enabled
```

Then in your chart's `values.yaml` specify the desired parameter overrides. Here is an example, showing a configuration that will backup the data files and generate database dumps. It will not make snapshots of the database files because the db `claimName` is commented out and thus empty by default.

```yaml
backups:
  enabled: true
  volume:
    nfs:
      basePath: "/taiga/ncsa/radiant/[project-code]/[cluster-name/backups/[app-name]"
      server: "nfs.server.hostname"
  data:
    enabled: true
    persistence:
      claimName: "my-app-data-pvc"
  db:
    enabled: true
    # persistence:
    #   claimName: "my-app-db-pvc"
    auth:
      host: "my-app-db"
      username: "dbuser"
      database: "dbname"
      existingSecret: "my-app-db-secret"
```

The backup jobs are controlled by Kubernetes CronJobs, executing by default once per night (UTC). You may adjust this schedule using standard cron notation.

See the [`values.yaml` file](./values.yaml) for detailed documentation of the Helm chart parameters.

## Restore

To restore data, first set the `restore.enabled` parameter to `true` and update your deployment. This will launch a persistent container that has both the backup volume (`/backup`) and the live data file volumes (`/restore`) mounted. Use `kubectl exec` to open a terminal to this container where you can see these mounted volumes. The following shows an example of the most recent snapshots (`snapshot.0`) of the data files and of the database dump file (your precise pod name will be different):

```bash
$ kubectl exec -it restore-76c5bbf874-9p6jk -- bash

$ root@restore-76c5bbf874-9p6jk:/# ls -l /backup/$DATA_PVC_NAME/snapshot.0/snapshots/data/
drwxrwxrwx.  2 www-data root 4096 Feb  5 20:41 config
drwxrwx---.  9 www-data root 4096 Feb 10 16:50 data
...

$ root@restore-76c5bbf874-9p6jk:/# ls -l /backup/$DB_HOST/$YYYYMMDD
-rw-r--r--. 1 root root 91944 Mar  2 17:06 $DB_NAME.$YYYYMMDDHHMMSS.dump

```

To restore data files, you could use `rsync` to overwrite the target files:

```bash
# View the differences and verify they are reasonable and expected
diff -rq /restore/files/data/ /backup/$DATA_PVC_NAME/snapshot.0/snapshots/data/
# Overwrite the application data with the backup snapshot files
rsync -van --delete /backup/$DATA_PVC_NAME/snapshot.0/snapshots/data/ /restore/files/data/
```

To restore the database from a dump file, use a command similar to the examples below, depending on whether the target database is PostgreSQL or MySQL. The database credentials and hostname environment variables are already populated.

```bash
# PostgreSQL
PGPASSWORD="${DB_PASS}" psql --host=$DB_HOST --dbname=$DB_NAME --username=$DB_USER < /backup/$DB_HOST/$YYYYMMDD/$DB_NAME.$YYYYMMDDHHMMSS.dump

# MySQL
mysql --host=$DB_HOST --user=$DB_USER --password="${DB_PASS}" \
    --database=$DB_NAME < /backup/$DB_HOST/$YYYYMMDD/$DB_NAME.$YYYYMMDDHHMMSS.sql
```

## Notes

By default the database files are not included in a snapshot, because you are not guaranteed to have a consistent database state when the files are copied this way. The database dump method is the safest way to ensure a quality backup. There may be cases in which a simple snapshot of the database files is preferable, so it is supported.

To reduce the risk of inconsistency between the database dump and the data files snapshot, schedule the CronJobs to execute at the same time.
