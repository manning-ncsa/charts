#!/bin/bash
set -e
#
# Required environment variable: MAX_SNAPSHOTS (max number of snapshots to retain)
#
NUM_SNAPSHOTS=0
# Prune snapshots by scanning the past 180 days and deleting all but the most recent $MAX_SNAPSHOTS snapshots
for days in $(seq 1 180); do
  SNAPSHOT_DIR=/backup/$(date --date="-$days days" +%Y%m%d)
  if [[ -d $SNAPSHOT_DIR ]]; then
    NUM_SNAPSHOTS=$((NUM_SNAPSHOTS+1))
    if (( $NUM_SNAPSHOTS >= $MAX_SNAPSHOTS )); then
      echo "Deleting backup:" $SNAPSHOT_DIR
      rm -rf $SNAPSHOT_DIR
    fi
  fi
done
