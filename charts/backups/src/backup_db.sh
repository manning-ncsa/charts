#!/bin/bash
set -e
#
# Required environment variables: 
# - MAX_SNAPSHOTS (max number of snapshots to retain)
# - DB_TYPE
# - DB_NAME
# - DB_HOST
# - DB_USER
# - DB_PASS
#
# Create YYYYMMDD backup subfolder and sync the current data to it
BACKUP_DIR=/backup/$(date +"%Y%m%d")
mkdir -p $BACKUP_DIR
# Dump the database to a file
if [[ $DB_TYPE == "postgresql" ]]; then
  # ref: https://www.postgresql.org/docs/current/app-pgdump.html
  export PGPASSWORD="$DB_PASS"
  pg_dump --clean --no-acl --host=$DB_HOST --dbname=$DB_NAME --username=$DB_USER > $BACKUP_DIR/$DB_NAME.$(date +"%Y%m%d%H%M%S").dump
fi
if [[ $DB_TYPE == "mysql" ]]; then
  mysqldump --column-statistics=0 --host=$DB_HOST --user=$DB_USER --password=$DB_PASS $DB_NAME > $BACKUP_DIR/$DB_NAME.$(date +"%Y%m%d%H%M%S").sql
fi
# Prune snapshots
bash /scripts/prune_snapshots.sh
